package routing

import akka.actor.{ActorSystem, Props}

object RoutingPool extends App {
  val system = ActorSystem("router")
  val router = system.actorOf(Props[RouterPool], "routes")

  router ! Work()
  router ! Work()
  router ! Work()
  router ! Work()
  router ! Work()

  Thread.sleep(100)

  system.terminate()
}
