package routing

import akka.actor.{ActorSystem, Props}

object RoutingGroup extends App {

  val system = ActorSystem("Routing-Group")
  system.actorOf(Props[Worker], "w1")
  system.actorOf(Props[Worker], "w2")
  system.actorOf(Props[Worker], "w3")
  system.actorOf(Props[Worker], "w4")
  system.actorOf(Props[Worker], "w5")

  val workerList = List("/user/w1", "/user/w2", "/user/w3", "/user/w4", "/user/w5")
  val routerGroup = system.actorOf(Props(classOf[RouterGroup], workerList))

  routerGroup ! Work()
  routerGroup ! Work()
  routerGroup ! Work()
  routerGroup ! Work()
  routerGroup ! Work()

  Thread.sleep(100)
  system.terminate()
}
