package routing

import akka.actor.{Actor, ActorRef, Props}

class RouterPool extends Actor {

  var routees: List[ActorRef] = _

  override def preStart(): Unit = {
    routees = List.fill(5)(context.actorOf(Props[Worker]))
  }

  override def receive: Receive = {
    case msg:Work =>
      println("Router: Received Message")
      routees(util.Random.nextInt(routees.size)) forward msg
  }
}

class RouterGroup(routees: List[String]) extends Actor {
  override def receive: Receive = {
    case msg: Work =>
      println(s"RouterGroup: Received work msg")
      context.actorSelection(routees(util.Random.nextInt(routees.size))) forward msg
  }
}
