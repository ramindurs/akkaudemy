package routing

import akka.actor.Actor

case class Work()

class Worker extends Actor{

  override def receive: Receive = {
    case msg:Work => println(s"Worker: Received [$msg]. My actor reference: [$self]")
  }
}
