package finitestatemachine

import akka.actor.{ActorSystem, FSM, Props, Stash}
import common.DBOperation.{Create, Update}
import common.{Connect, DisConnect, Operation, UserRepository}
import finitestatemachine.UserStorageFSM.{Connected, Disconnected, EmptyData}

object UserStorageFSM {

  sealed trait State

  case object Connected extends State

  case object Disconnected extends State

  sealed trait Data

  case object EmptyData extends Data

}

class UserStorageFSM extends FSM[UserStorageFSM.State, UserStorageFSM.Data] with Stash {

  // 1. Define StartWith
  startWith(Disconnected, EmptyData)

  // 2. Define States that receive events
  // a) Disconnected state
  when(Disconnected) {
    case Event(Connect, _) =>
      println("UserStorage: Connected to DB")
      unstashAll()
      goto(Connected) using EmptyData
    case Event(_, _) =>
      println("UserStorage: Cannot handle in disconnected state")
      stash()
      stay using EmptyData
  }

  // b) Connected State
  when(Connected) {
    case Event(DisConnect, _) =>
      println("UserStorage: Disconnected from DB")
      goto(Disconnected) using EmptyData
    case Event(Operation(op, user), _) =>
      println(s"UserStorage: Received $op for $user")
      user.foreach(u => UserRepository.db(op, u))
      stay using EmptyData
    case Event(a@_, b@_) =>
      println(s"UserStorage: Cannot handle [$a] [$b]")
      stay using EmptyData
  }

  // 3. initialise
  initialize()
}


object FSM extends App {

  import UserRepository._

  val system = ActorSystem("HotSwap-FSM")
  val userStorage = system.actorOf(Props[UserStorageFSM], "userStorage")

  userStorage ! Operation(Create, Some(adam))
  userStorage ! Connect
  userStorage ! Operation(Update, Some(eve))
  userStorage ! DisConnect

  Thread.sleep(100)
  system.terminate()
}
