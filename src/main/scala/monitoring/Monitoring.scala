package monitoring

import akka.actor.{Actor, ActorRef, ActorSystem, Props, Terminated}

class Ares(athena: ActorRef) extends Actor {

  override def preStart(): Unit = {
    context.watch(athena)
  }

  override def postStop(): Unit = {
    println(s"Ares: postStop")
  }

  override def receive: Receive = {
    case Terminated => context.stop(self)
  }
}

class Athena extends Actor {
  override def receive: Receive = {
    case msg =>
      println(s"Athena: Received $msg")
      context.stop(self)
  }
}

object Monitoring extends App {

  val system = ActorSystem("monitoring")
  val athena = system.actorOf(Props[Athena], "athena")
  val ares = system.actorOf(Props(new Ares(athena)), "ares")

  athena ! "Hello Athena"

  Thread.sleep(1000)
  system.terminate()
}
