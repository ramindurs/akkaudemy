package music

import akka.actor.{Actor, ActorSystem, Props}
import music.MusicController.{Play, Stop}
import music.MusicPlayer.{StartMusic, StopMusic}

object MusicController {

  sealed trait ControllerMsg
  case object Play extends ControllerMsg
  case object Stop extends ControllerMsg

  def props = Props[MusicController]
}

class MusicController extends Actor {
  override def receive: Receive = {
    case Play =>
      println("Music started...")
    case Stop =>
      println("Music stopped...")
  }
}

object MusicPlayer {
  sealed trait PlayMsg
  case object StopMusic extends PlayMsg
  case object StartMusic extends PlayMsg

  def props = Props[MusicPlayer]
}

class MusicPlayer extends Actor {
  override def receive: Receive = {
    case StartMusic => println("I don't want to stop music")
    case StopMusic =>
      val controller = context.actorOf(MusicController.props, "controller")
      controller ! Play
    case _ => println("Unknown Message")
  }
}

object MusicRunner extends App {
  val system = ActorSystem("Music-Controller")
  val player = system.actorOf(MusicPlayer.props, "player")
  player ! StartMusic
  player ! StopMusic

  system.terminate()
}