package registration

import akka.actor.{Actor, ActorRef, Props}
import akka.util.Timeout
import registration.Checker.{BlackUser, WhiteUser}
import registration.Recorder.NewUser
import registration.Storage.AddUser

import scala.concurrent.duration._

object Recorder {

  sealed trait RecorderMessage

  case class NewUser(user: User) extends RecorderMessage

  def props(checker: ActorRef, storage: ActorRef): Props = Props(new Recorder(checker, storage))

}

class Recorder(checker: ActorRef, storage: ActorRef) extends Actor {

  import scala.concurrent.ExecutionContext.Implicits.global
  import akka.pattern.ask

  implicit val timeout: Timeout = Timeout(5 seconds)

  override def receive: Receive = {
    case NewUser(user) =>
      checker ? CheckUser(user) map {
        case WhiteUser(wu) => storage ! AddUser(wu)
        case BlackUser(bu) => println(s"Recorder: Blacklisted user: $bu")
        case a@_ => println(s"Recorder: $a not supported")
      }
    case a@_ => println(s"Recorder: Message recieved not supported: $a")
  }
}
