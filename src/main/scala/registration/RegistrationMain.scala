package registration

import akka.actor.{ActorRef, ActorSystem, Props}
import registration.Recorder.NewUser

object RegistrationMain extends App {

  val system = ActorSystem("User-Registration")

  val checker: ActorRef = system.actorOf(Checker.props, "checker")
  val storage: ActorRef = system.actorOf(Storage.props, "storage")

  val recorder: ActorRef = system.actorOf(Recorder.props(checker, storage), "recorder")

  recorder ! NewUser(User("Eve", "eve@eden.com"))

  Thread.sleep(1000)

  recorder ! NewUser(User("Ramindur", "ramindur@eden.com"))

  Thread.sleep(1000)
  system.terminate()
}
