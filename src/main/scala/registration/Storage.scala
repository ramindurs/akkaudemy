package registration

import akka.actor.{Actor, Props}
import registration.Storage.AddUser

object Storage {

  sealed trait StorageMessage

  case class AddUser(user: User) extends StorageMessage

  def props: Props = Props[Storage]

}

class Storage extends Actor {

  var users = List.empty[User]

  override def receive: Receive = {
    case AddUser(user) =>
      println(s"Storage: Saving $user")
      users = user :: users
    case a@_ => println(s"Storage: $a is not supported")
  }
}
