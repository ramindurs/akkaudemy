package registration

import akka.actor.{Actor, Props}
import registration.Checker.{BlackUser, CouldNotCheck, WhiteUser}

case class CheckUser(user: User)

object Checker {

  sealed trait CheckerMsg

  case class CheckUser(user: User) extends CheckerMsg

  sealed trait CheckerResponse

  case class BlackUser(user: User) extends CheckerResponse

  case class WhiteUser(user: User) extends CheckerResponse

  case class CouldNotCheck() extends CheckerResponse

  def props: Props = Props[Checker]
}

class Checker extends Actor {

  val blacklist = List(User("Adam", "adam@eden.com"), User("Eve", "eve@eden.com"))

  override def receive: Receive = {
    case CheckUser(user) =>
      if (blacklist.contains(user))
        sender() ! BlackUser(user)
      else
        sender() ! WhiteUser(user)
    case a@_ => println(s"Checker: $a not supported")
      sender() ! CouldNotCheck()
  }
}
