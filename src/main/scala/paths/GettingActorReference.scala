package paths

import akka.actor.{Actor, ActorIdentity, ActorRef, ActorSelection, ActorSystem, Identify, Props}

class Watcher extends Actor {

  val selection: ActorSelection = context.actorSelection("/user/counter")
  var counterRef: ActorRef =  _
  selection ! Identify(None)

  override def receive: Receive = {
    case ActorIdentity(_, Some(value)) =>
      println(s"Actor Reference for counter: $value")
    case ActorIdentity(_, None) =>
      println("Actor Reference not found")
  }
}

object GettingActorReference extends App {
  val system = ActorSystem("Watch-actor-selection")
  val counter = system.actorOf(Props[Counter], "counter")
  val watcher = system.actorOf(Props[Watcher], "watcher")

  Thread.sleep(1000)

  system.terminate()
}
