package paths

import akka.actor.Actor

trait Change

case class Inc(num: Int) extends Change

case class Dec(num: Int) extends Change

class Counter extends Actor {

  var counterADT = CounterADT(0)

  override def receive: Receive = {
    case i@Inc(num) => counterADT = CounterADT.change(i, counterADT)
    case d@Dec(num) => counterADT = CounterADT.change(d, counterADT)
    case a@_ => println(s"Counter: Not supported [$a]")
  }
}