package paths

import akka.actor.{Actor, ActorSystem, PoisonPill, Props}

object Actors extends App {

  val system = ActorSystem("Actor-Paths")
  val counter1 = system.actorOf(Props[Counter], "counter")
  println(s"Actor Reference: $counter1")

  val counterSelection = system.actorSelection("counter")
  println(s"Actor Selection: $counterSelection")

  Thread.sleep(1000)

  counter1 ! PoisonPill

  Thread.sleep(100)

  val counter2 = system.actorOf(Props[Counter], "counter")
  println(s"Actor Reference: $counter2")
  val counterSelection2 = system.actorSelection("counter")
  println(s"Actor Selection: $counterSelection2")

  system.terminate()
}
