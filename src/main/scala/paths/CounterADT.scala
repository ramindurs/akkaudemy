package paths

case class CounterADT(acc: Int)

object CounterADT {

  def apply(acc: Int): CounterADT = new CounterADT(acc)

  def change(change: Change, counterADT: CounterADT): CounterADT = change match {
    case Inc(num) => CounterADT(counterADT.acc + num)
    case Dec(num) => CounterADT(counterADT.acc - num)
  }

}
