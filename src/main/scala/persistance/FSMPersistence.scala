package persistance

import akka.actor.{ActorSystem, Props}
import akka.persistence.fsm.PersistentFSM
import akka.persistence.fsm.PersistentFSM.FSMState
import persistance.Account.{AcceptedTransaction, Active, Balance, Credit, Data, Debit, DomainEvent, Empty, Operation, Query, RejectedTransaction, State, TransactionType, ZeroBalance}

import scala.reflect.ClassTag

object Account {

  // Account States
  sealed trait State extends FSMState

  case class Empty() extends State {
    override def identifier: String = "EmptyState"
  }

  case class Active() extends State {
    override def identifier: String = "ActiveState"
  }

  // Account Data
  sealed trait Data {
    val amount: Float
  }

  case object ZeroBalance extends Data {
    override val amount: Float = 0.0F
  }

  case class Balance(override val amount: Float) extends Data

  // Domain events (Persist events)
  sealed trait DomainEvent

  case class AcceptedTransaction(amount: Float, `type`: TransactionType) extends DomainEvent

  case class RejectedTransaction(amount: Float, `type`: TransactionType, reason: String) extends DomainEvent

  sealed trait TransactionType

  case object Credit extends TransactionType

  case object Debit extends TransactionType

  case object Query extends TransactionType

  // Commands
  case class Operation(amount: Float, `type`: TransactionType)

}

class Account extends PersistentFSM[State, Data, DomainEvent] {

  import scala.reflect.classTag

  override def domainEventClassTag: ClassTag[DomainEvent] = classTag[DomainEvent]

  override def applyEvent(domainEvent: DomainEvent, currentData: Data): Data = {
    domainEvent match {
      case AcceptedTransaction(amount, Credit) =>
        val balance = Balance(currentData.amount + amount)
        println(s"Account: New Balance: [$balance]")
        balance
      case AcceptedTransaction(amount, Debit) =>
        val balance = Balance(currentData.amount - amount)
        if (balance.amount <= 0) {
          println(s"Account: Zero Balance with [$balance]")
          ZeroBalance
        } else {
          println(s"Account: New Balance: [$balance]")
          balance
        }
      case RejectedTransaction(_, _, reason) =>
        println(s"Account: Rejected transaction with [$reason]")
        currentData
    }
  }

  override def persistenceId: String = "account-fsm-persistance"

  startWith(Empty(), ZeroBalance)

  when(Empty()) {
    case Event(Operation(amount, Credit), _) =>
      println(s"Account: First Credit with [$amount]")
      goto(Active()) applying AcceptedTransaction(amount, Credit)
    case Event(Operation(amount, Debit), _) =>
      println(s"Account: Cannot debit a zero balance")
      stay applying RejectedTransaction(amount, Debit, "Balance is 0")
  }

  when(Active()) {
    case Event(Operation(amount, Credit), _) =>
      println(s"Account: Credited with [$amount]")
      stay applying AcceptedTransaction(amount, Credit)
    case Event(Operation(_, Query), balance) =>
      println(s"Account: Current Balance: [${balance.amount}]")
      stay
    case Event(Operation(amount, Debit), currentBalance) =>
      val newBalance = currentBalance.amount - amount
      if (newBalance >= 0) {
        println(s"Account: Account debited by [$amount]")
        stay applying AcceptedTransaction(amount, Debit)
      } else {
        println(s"Account: You do not have sufficent funds to debit [$amount]")
        stay applying RejectedTransaction(amount, Debit, "Insufficient funds")
      }
  }
}

object FSMPersistence extends App {
  val system = ActorSystem("fsm-persistence")
  val account = system.actorOf(Props[Account], "account")

  account ! Operation(10.00F, Debit)
  account ! Operation(100.00F, Credit)
  account ! Operation(10.00F, Debit)
  account ! Operation(10.00F, Debit)
  account ! Operation(5.00F, Credit)
  account ! Operation(0F, Query)

  Thread.sleep(2000)
  system.terminate()
}
