package persistance

import akka.actor.{ActorSystem, Props}
import akka.persistence._
import persistance.Counter._

object Counter {

  sealed trait Operation {
    val count: Int
  }

  case class Increment(override val count: Int) extends Operation

  case class Decrement(override val count: Int) extends Operation

  case class Cmd(op: Operation)

  case class Evnt(op: Operation)

  case class State(count: Int)

}

class Counter extends PersistentActor {

  var state: State = State(count = 0)

  def updateState(evt: Evnt): Unit = evt match {
    case Evnt(Increment(count)) => state =
      State(count = state.count + count)
      takeSnapShot()
    case Evnt(Decrement(count)) =>
      state = State(count = state.count - count)
      takeSnapShot()
    case a@_ => println(s"Counter: UpdateState cannot handle [$a]")
  }

  override def receiveRecover: Receive = {
    case event: Evnt =>
      println(s"Counter: Received $event in recovering mood")
      updateState(event)
    case SnapshotOffer(_, snapshot: State) =>
      println(s"Counter: Received snapshot with State [$snapshot] in recovering mood")
      state = snapshot
    case RecoveryCompleted =>
      println("Counter: Recovery completed")
    case a@_ => println(s"Counter: Cannot handle [$a] in recovering mood")
  }

  override def receiveCommand: Receive = {
    case cmd@Cmd(op) =>
      println(s"Counter: Received CMD[$cmd] in Normal mood")
      persist(Evnt(op)) {
        evnt => updateState(evnt)
      }
    case "print" => println(s"Counter: Current state [$state]")
    case SaveSnapshotSuccess(metadata) =>
      println(s"Counter: Save snapshot succeeded: [$metadata]")
    case SaveSnapshotFailure(md, cause) =>
      println(s"Counter: Saved snapshot failure: [$md]")
  }

  def takeSnapShot(): Unit = {
    if (state.count % 5 == 0)
      saveSnapshot(state)
  }

  override def persistenceId: String = "counter-persistance"

  override def recovery: Recovery = Recovery.create()
}

object PersistanceActor extends App {

  val system = ActorSystem("persistent-actor")
  val counter = system.actorOf(Props[Counter], "counter")

  counter ! 3
  counter ! Cmd(Increment(1))
  counter ! Cmd(Decrement(3))
  counter ! Cmd(Increment(1))

  counter ! "print"

  Thread.sleep(2000)
  system.terminate()
}
