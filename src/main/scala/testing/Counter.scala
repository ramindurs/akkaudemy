package testing

import akka.actor.Actor
import testing.Counter.{Decrement, GetCount, Increment}

object Counter {

  trait Action

  case object Increment extends Action

  case object Decrement extends Action

  case object GetCount extends Action

}

class Counter extends Actor {

  var count: Int = 0

  override def receive: Receive = {
    case Increment => count += 1
    case Decrement => count -= 1
    case GetCount => sender ! count
  }
}
