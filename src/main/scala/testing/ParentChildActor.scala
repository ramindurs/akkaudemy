package testing

import akka.actor.{Actor, ActorRef, ActorRefFactory, ActorSystem, Props}
import akka.util.Timeout

import scala.concurrent.duration._


class Child(parent: ActorRef) extends Actor {

  override def receive: Receive = {
    case "ping" =>
      println("Child: Received Ping, Sending back pong")
      parent ! "pong"
    case a@_ => println(s"Child: cannot understand $a")
  }
}

class Parent(childMaker: ActorRefFactory => ActorRef) extends Actor {
  implicit val timeout: Timeout = Timeout(5 seconds)

  val child: ActorRef = childMaker(context)
  var ponged = false

  override def receive: Receive = {
    case "ping" => child ! "ping"
    case "pong" =>
      println(s"Parent: Received pong")
      ponged = true
    case a@_ =>
      println(s"Parent: Cannot understand [$a]")
  }
}

object ParentChildActor extends App {
  implicit val timeout: Timeout = Timeout(5 seconds)

  val system = ActorSystem("parent-child")

  val parent = system.actorOf(Props(new Parent(system)), "parent")
//  parent ? "ping" map {
//    case "pong" => println(s"ParentChild: Sent a ping and  got a pong")
//    case a@_ => println(s"ParentChild: Sent a ping and got $a")
//  }

  Thread.sleep(1000)
  system.terminate()
}
