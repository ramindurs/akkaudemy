package behaviour

import akka.actor.{Actor, ActorSystem, Props, Stash}
import common.DBOperation.Create
import common._

class UserStorage extends Actor with Stash {

  def connected: Actor.Receive = {
    case DisConnect =>
      println(s"User Storage DisConnected from DB")
      context.unbecome()
    case Operation(op, user) =>
      println(s"User Storage Received $op on $user")
      user.foreach(u => UserRepository.db(op, u))
    case _ => println(s"Cannot handle in connected state")
  }

  def disconnected: Actor.Receive = {
    case Connect =>
      println(s"User Storage Connected to DB")
      unstashAll()
      context.become(connected)
    case a@_ =>
      println(s"Cannot handle [$a] in disconnected state, stashing")
      stash()
  }

  override def receive: Receive = disconnected
}

object BecomeHotSwap extends App {
  import UserRepository._

  val system = ActorSystem("Hotswap-Become")
  val userStorage = system.actorOf(Props[UserStorage], "userStorage")

  userStorage ! Operation(Create, Some(adam))
  userStorage ! Connect
  userStorage ! Operation(Create, Some(eve))
  userStorage ! DisConnect

  Thread.sleep(100)
  system.terminate()
}