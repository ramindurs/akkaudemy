package common

case class User(name: String, email: String)

object UserRepository {
  val adam = User("Adam", "adam@eden.gdn")
  val eve = User("Eve", "eve@eden.gdn")

  def db[User](op: DBOperation, user: User): Unit = {
    println(s"Carrying out $op on $user")
  }
}
