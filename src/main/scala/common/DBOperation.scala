package common

trait DBOperation

object DBOperation {

  case object Create extends DBOperation

  case object Update extends DBOperation

  case object Read extends DBOperation

  case object Delete extends DBOperation

}

case object Connect

case object DisConnect

case class Operation[T](dbOperation: DBOperation, data: Option[T])