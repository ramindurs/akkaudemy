package supervision

import akka.actor.SupervisorStrategy.{Escalate, Restart, Resume, Stop}
import akka.actor.{Actor, ActorRef, ActorSystem, OneForOneStrategy, Props}

import scala.concurrent.duration._

sealed trait Message

case object ResumeMsg extends Message

case object StopMsg extends Message

case object RestartMsg extends Message

case object ResumeException extends Exception

case object StopException extends Exception

case object RestartException extends Exception


class Aphrodite extends Actor {

  override def preStart() = {
    println("Aphrodite: preStart hook")
    super.preStart()
  }

  override def preRestart(reason: Throwable, message: Option[Any]) = {
    println(s"Aphrodite: preRestart: [$reason]; [$message]")
    super.preRestart(reason, message)
  }

  override def postRestart(reason: Throwable): Unit = {
    println(s"Aphrodite: postRestart: [$reason]")
    super.postRestart(reason)
  }

  override def postStop(): Unit = {
    println("Aphrodite: postStop")
    super.postStop()
  }

  override def receive: Receive = {
    case ResumeMsg => throw ResumeException
    case StopMsg => throw StopException
    case RestartMsg => throw RestartException
    case _ => throw new Exception
  }
}

object Aphrodite {
  def props: Props = Props[Aphrodite]
}

class Hera extends Actor {

  var childRef: ActorRef = _

  override val supervisorStrategy: OneForOneStrategy =
    OneForOneStrategy(maxNrOfRetries = 10, withinTimeRange = 1 second) {
      case ResumeException => Resume
      case RestartException => Restart
      case StopException => Stop
      case _: Exception => Escalate
    }

  override def preStart(): Unit = {
    println("Hera: preStart")
    childRef = context.actorOf(Aphrodite.props, "aphrodite")
    super.preStart()
    Thread.sleep(1000)
  }

  override def receive: Receive = {
    case msg =>
      println(s"Hera: Received $msg")
      childRef ! msg
      Thread.sleep(100)
  }
}

object Hera {
  def props: Props = Props[Hera]
}

object Supervision extends App {
  val system = ActorSystem("supervision")
  val hera = system.actorOf(Hera.props, "hera")

  hera ! StopMsg
  Thread.sleep(2000)

  println("Stopping....")
  system.terminate()
}
