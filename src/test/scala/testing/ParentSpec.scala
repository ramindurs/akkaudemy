package testing

import akka.actor.{ActorRef, ActorRefFactory, ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import org.scalatest.{BeforeAndAfterAll, FlatSpecLike, MustMatchers}

class ParentSpec extends TestKit(ActorSystem("child-test"))
  with ImplicitSender
  with FlatSpecLike
  with BeforeAndAfterAll
  with MustMatchers {

  override def afterAll(): Unit = TestKit.shutdownActorSystem(system)

  "Parent actor" should "send a ping message to child when it receives a ping message" in {
    val child = TestProbe()
    val childMaker: ActorRefFactory => ActorRef = (_: ActorRefFactory) => child.ref

    val parent = system.actorOf(Props(new Parent(childMaker)))

    parent ! "ping"

    child.expectMsg("ping")
  }

}
