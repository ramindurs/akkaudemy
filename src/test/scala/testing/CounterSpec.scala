package testing

import akka.actor.{ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import org.scalatest.{BeforeAndAfterAll, FlatSpec, FlatSpecLike, MustMatchers}
import testing.Counter.{Decrement, GetCount, Increment}

class CounterSpec extends TestKit(ActorSystem("test-counter"))
  with ImplicitSender
  with FlatSpecLike
  with BeforeAndAfterAll
  with MustMatchers {

  override def afterAll(): Unit = TestKit.shutdownActorSystem(system)

  "Counter Actor" should "handle GetCount message with using TestProbe" in {
    val sender = TestProbe()
    val counter = system.actorOf(Props[Counter])

    sender.send(counter, Increment)
    sender.send(counter, GetCount)

    val state = sender.expectMsgType[Int]

    state must equal(1)
  }

  it should "handle increment message" in {
    val counter = system.actorOf(Props[Counter])

    counter ! Increment

    expectNoMessage()
  }

  it should "handle decrement message" in {
    val counter = system.actorOf(Props[Counter])

    counter ! Decrement

    expectNoMessage()
  }

  it should "handle get count message" in {
    val counter = system.actorOf(Props[Counter])

    counter ! GetCount

    expectMsg(0)
  }

  it should "handle sequence of messages" in {
    val counter = system.actorOf(Props[Counter])

    counter ! Increment
    counter ! Increment
    counter ! Decrement
    counter ! Increment
    counter ! GetCount

    expectMsg(2)
  }

}
